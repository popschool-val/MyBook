json.extract! borrow, :id, :book_id, :user_id, :returned_at, :state, :created_at, :updated_at
json.url borrow_url(borrow, format: :json)
