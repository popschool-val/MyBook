class Borrow < ApplicationRecord
  belongs_to :book
  belongs_to :user
  enum state: %i[local home]
end
