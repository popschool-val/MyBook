class Book < ApplicationRecord
  belongs_to :category
  has_many :borrows
  has_many :users, through: :borrows

  enum state: %i[available local home]
end
