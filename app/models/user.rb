class User < ApplicationRecord
  has_many :borrows
  has_many :books, through: :borrows

  def full_name
    "#{first_name} #{last_name}"
  end
end
