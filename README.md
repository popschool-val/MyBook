# 1 etape de creation
```
rails new MyBook
cd MyBook
yarn init
rails g scaffold category name:string
rails g scaffold book title:string content:text category:references state:integer
rails db:create
rails db:migrate
rails g scaffold user first_name:string last_name:string age:integer enable:boolean
rails db:migrate
rails g scaffold borrow book:references user:references returned_at:datetime state:integer
rails db:migrate
rails s
bundle update
rails db:seed
rails s
rails c
```

# 2 utilisation
```
git clone ....
cd MyBook
(rename databas.yml.sample to database.yml)
bundle install
rake db:create
rake db:migrate
rails s
```
