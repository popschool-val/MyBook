class CreateBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :books do |t|
      t.string :title
      t.text :content
      t.references :category, foreign_key: true
      t.integer :state

      t.timestamps
    end
  end
end
