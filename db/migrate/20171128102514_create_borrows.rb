class CreateBorrows < ActiveRecord::Migration[5.1]
  def change
    create_table :borrows do |t|
      t.references :book, foreign_key: true
      t.references :user, foreign_key: true
      t.datetime :returned_at
      t.integer :state

      t.timestamps
    end
  end
end
