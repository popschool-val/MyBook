# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

if User.all.count.zero?
  (1..40).each do |_n|
    User.create!(first_name:  Faker::Name.first_name,
                 last_name:   Faker::Name.last_name,
                 age:         Faker::Number.between(1, 99),
                 enable:      Faker::Boolean.boolean)
  end
end

if Category.all.count.zero?
  (1..10).each do |_n|
    Category.create!(name: Faker::Book.genre)
  end
end

if Book.all.count.zero?
  (1..200).each do |_n|
    Book.create!(title:         Faker::Book.title,
                 content:       Faker::Lorem.paragraph,
                 category:      Category.all.sample,
                 state:        Faker::Number.between(0, 2))
  end
end

if Borrow.all.count.zero?
  (1..300).each do |_n|
    Borrow.create!(book:     Book.all.sample,
                   user:     User.all.sample,
                   state: Faker::Number.between(0, 1))
  end
end
